#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define STUD_NAME "Mihkel Tääkre"
#define STUD_NAME1 "Mihkel T"
#define STUD_NAME2 0xE1
#define STUD_NAME3 "kre"
#define notNR "Not a number!"
#define notNR1 "Invalid input!"
#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " " __TIME__
#define VER_LIBC_GCC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__
#define ENTRD_NR "You entered number %d"
#define notNR2 "Please enter number between 0 and 9!"
#define notNR3 "Argument is not a decimal number!"

#define CLI_HELP_MSG "Implemented commands:"
#define UPTIME_MSG "Uptime"
#define PRT_CMD_ERR "Command not implemented.\r\n\tUse <help> to get help.\r\n"
#define PRT_ARG_ERR "To few or too many arguments for this command\r\n\tUse <help>\r\n"
#define CLI_EX "Command had following arguments:\r\n"
#define PRT_HELP "Implemented commands:\r\n"
#define HELP_CMD "help"
#define HELP_HELP "Get help"
#define EX_CMD "example"
#define EX_HELP "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>"
#define VER_CMD "version"
#define VER_HELP "Print FW version"
#define ASCII_CMD "ascii"
#define ASCII_HELP "print ASCII tables"
#define NUMBER_CMD "number"
#define NUMBER_HELP "Print and display matching number Usage: number <decimal number>"

extern PGM_P const numbers[];

extern const char help_cmd[];
extern const char help_help[];
extern const char example_cmd[];
extern const char example_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char ascii_cmd[];
extern const char ascii_help[];
extern const char number_cmd[];
extern const char number_help[];

#endif /* _HMI_MSG_H_ */
