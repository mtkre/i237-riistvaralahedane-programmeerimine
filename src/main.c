#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "cli_microrl.h"

#define UART_BAUD 9600
#define F_CPU 16000000UL
#define UART_STATUS_MASK 0x00FF

#define LED_INIT DDRA |= _BV(DDA0);
#define LED_TOGGLE PORTA ^= _BV(PORTA0)


// Create microrl object and pointer on it
static microrl_t rl;
static microrl_t *prl = &rl;

volatile uint32_t counter_1;

int uart0_putchar_w(char c, FILE *stream)
{
    (void) stream;

    if (c == '\n') {
        uart0_putc('\r');
        uart0_putc('\n');
        return 0;
    }

    uart0_putc( c);
    return 0;
}

int uart0_getchar_w(FILE *stream)
{
    (void) stream;

    while (uart0_peek() == UART_NO_DATA);

    return (uart0_getc() & UART_STATUS_MASK);
}

int uart3_putchar_w(char c, FILE *stream)
{
    (void) stream;

    if (c == '\n') {
        uart3_putc('\r');
        uart3_putc('\n');
        return 0;
    }

    uart3_putc(c);
    return 0;
}

FILE uart0_io = FDEV_SETUP_STREAM(uart0_putchar_w, uart0_getchar_w,
                                  _FDEV_SETUP_RW);
FILE uart3_out = FDEV_SETUP_STREAM(uart3_putchar_w, NULL, _FDEV_SETUP_WRITE);

static inline void init_hw_cli (void)
{
    // IO init
    /// Set arduino pin 22 as output
    LED_INIT;
    // UART init
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    stdout = stdin = &uart0_io;
    stderr = &uart3_out;
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);
    // LCD init
    lcd_init();
    lcd_clrscr();
    // Enable interupts
    sei();
}

static inline void init_uart_cli(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    stderr = &uart3_out;
    stdout = stdin = &uart0_io;
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);
}


static inline void start_ui (void)
{
    fprintf_P(stdout, PSTR(STUD_NAME"\n"));
    fprintf_P(stdout, PSTR(">"));
    lcd_puts_P(PSTR(STUD_NAME1));
    lcd_putc(STUD_NAME2);
    lcd_putc(STUD_NAME2);
    lcd_puts_P(PSTR(STUD_NAME3));
}

static inline void init_counter(void)
{
    counter_1 = 0;
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output compare a match interrupt enable
}

static inline void heartbeat(void)
{
    static uint32_t time_y2k_prev;
    uint32_t time_y2k_cpy;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        time_y2k_cpy = counter_1;
    }

    if (time_y2k_cpy != time_y2k_prev) {
        fprintf_P(stderr, PSTR(UPTIME_MSG": %lu s\n"), time_y2k_cpy);
        time_y2k_prev = time_y2k_cpy;
        LED_TOGGLE;
    }
}



void main(void)
{
    init_hw_cli();
    init_counter();
    start_ui();

    while (1) {
        heartbeat();
        microrl_insert_char (prl, uart0_getc() & UART_STATUS_MASK);
    }
}

ISR(TIMER1_COMPA_vect)
{
    counter_1++;
}



