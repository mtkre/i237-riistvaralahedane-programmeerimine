#include <avr/pgmspace.h>
#include "hmi_msg.h"

const  char nr1[] PROGMEM = "One";
const  char nr2[] PROGMEM = "Two";
const  char nr3[] PROGMEM = "Three";
const  char nr4[] PROGMEM = "Four";
const  char nr5[] PROGMEM = "Five";
const  char nr6[] PROGMEM = "Six";
const  char nr7[] PROGMEM = "Seven";
const  char nr8[] PROGMEM = "Eight";
const  char nr9[] PROGMEM = "Nine";
const  char nr0[] PROGMEM = "Zero";

PGM_P const numbers[] PROGMEM = {nr0, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8, nr9};

const char help_cmd[] PROGMEM = HELP_CMD;
const char help_help[] PROGMEM = HELP_HELP;
const char example_cmd[] PROGMEM = EX_CMD;
const char example_help[] PROGMEM = EX_HELP;
const char ver_cmd[] PROGMEM = VER_CMD;
const char ver_help[] PROGMEM = VER_HELP;
const char ascii_cmd[] PROGMEM = ASCII_CMD;
const char ascii_help[] PROGMEM = ASCII_HELP;
const char number_cmd[] PROGMEM = NUMBER_CMD;
const char number_help[] PROGMEM = NUMBER_HELP;
