#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"

#define NUM_ELEMS(x) (sizeof(x) / sizeof((x)[0]))

typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
};


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(PRT_HELP));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR(CLI_EX));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(VER_FW"\r\n"));
    uart0_puts_p(PSTR(VER_LIBC_GCC"\r\n"));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    printf_P(PSTR("\n"));
    print_ascii_tbl(stdout);
    unsigned char cArray[128] = {0};

    for (unsigned char i = 0; i < sizeof(cArray); i++) {
        cArray[i] = i;
    }

    print_for_human(stdout, cArray, sizeof(cArray));
}


void cli_handle_number(const char *const *argv)
{
    (void) argv;
    int x = atoi(argv[1]);
    printf_P(PSTR("\n"));
    lcd_clr(LCD_ROW_2_START, 16);
    lcd_goto(LCD_ROW_2_START);

    for (size_t i = 0; i < strlen(argv[1]); i++) {
        if (!isdigit(argv[1][i])) {
            uart0_puts_p(PSTR(notNR3"\r\n"));
            lcd_puts_P(PSTR(notNR1));
            return;
        } else if (x == 1 || x == 2 || x == 3 || x == 4 || x == 5 || x == 6 ||
                   x == 7 || x == 8     || x == 9 || x == 0) {
            lcd_puts_P((PGM_P)pgm_read_word(&numbers[x]));
            fprintf_P(stdout, PSTR(ENTRD_NR"\n"), x);
        } else {
            uart0_puts_p(PSTR(notNR2"\r\n"));
            //fprintf_P(stdout, PSTR("\n"notNR2" \n"));
            lcd_puts_P(PSTR(notNR1));
            return;
        }
    }
}


void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR(PRT_CMD_ERR));
}


void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR(PRT_ARG_ERR));
}


int cli_execute(int argc, const char *const *argv)
{
    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}

